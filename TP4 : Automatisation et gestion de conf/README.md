# I. Premiers pas Ansible



## 1. Mise en place



- créez un fichier `.ssh-config` avec le contenu suivant

```ssh-config
Host 10.4.1.*
  User arthur
  IdentityFile ~/.ssh/id_rsa
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentitiesOnly yes
  LogLevel FATAL
```

- créez un fichier `ansible.cfg` avec le contenu suivant

```ini
[ssh_connection]
ssh_args = -F ./.ssh-config
```

## 2. La commande `ansible`



➜ Pour cela, **créez un fichier `hosts.ini`** avec le contenu suivant :

```ini
[web]
10.4.1.11
[tp1]
10.4.1.11
10.4.1.12
[db]
10.4.1.12
```



## 3. Un premier playbook


➜ **créez un fichier `first.yml`**, notre premier *playbook* :

```yaml
---
- name: Install nginx
  hosts: tp1
  become: true

  tasks:
  - name: Install nginx
    dnf:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started
```

➜ **Et un fichier `index.html.j2` dans le même dossier**

```jinja2
Hello from {{ ansible_default_ipv4.address }}
```

➜ **Exécutez le playbook**

```bash
$ ansible-playbook -i hosts.ini first.yml
```

## 3. Création de nouveaux playbooks

### A. NGINX

➜ **Créez un *playbook* `nginx.yml`**
index html
```
Hello from {{ ansible_default_ipv4.address }}
```
nginx conf
```
user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;
    log_format main '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log /var/log/nginx/access.log main;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;

    server {
        listen 443 ssl;
        server_name tata.com.efrei;

        ssl_certificate /etc/pki/tls/certs/server.crt;
        ssl_certificate_key /etc/pki/tls/private/server.key;

        root /var/www/tp2_site;
        index index.html;

        location / {
            try_files $uri $uri/ =404;
        }
    }
    server {
        listen 443 ssl;
        server_name toto.com.efrei;

        ssl_certificate /etc/pki/tls/certs/server.crt;
        ssl_certificate_key /etc/pki/tls/private/server.key;

        root /var/www/tp1_site;
        index index.html;

        location / {
            try_files $uri $uri/ =404;
        }
    }
}

```
ajout du server.crt et server.key dans le dossier

### B. MariaDB

➜ **Créez un *playbook* `mariadb.yml`**

mariadbl.yml
```
---
- name: Deploy MariaDB server and create user/database
  hosts: db
  become: true
  vars:
    mysql_user: "arthur_db"
    mysql_user_password: "toto"
    mysql_database: "salut"

  tasks:

    - name: Add pip binary
      dnf:
        name: python3-pip
        state: present

    - name: Add PyMySQL Python library coucou
      pip:
        name: PyMySQL
        state: present

    - name: Install MariaDB server
      dnf:
        name: mariadb-server
        state: present

    - name: Start MariaDB service
      service:
        name: mariadb
        state: started
        enabled: yes

    - name: Create MySQL database
      mysql_db:
        name: "{{ mysql_database }}"
        state: present
        login_user: root
        login_unix_socket: /var/lib/mysql/mysql.sock

    - name: Create MySQL user
      mysql_user:
        name: "{{ mysql_user }}"
        password: "{{ mysql_user_password }}"
        priv: "{{ mysql_database }}.*:ALL"
        login_user: root
        login_unix_socket: /var/lib/mysql/mysql.sock
```

# II. Range ta chambre



# Arborescence des fichiers

- tp
  - .vagrant
    - (tout les fichier de conf generer pas vagrant)
  - ansible
    - I3
       - first.yml
       - index.html.j2

    - I4A
      - index.html.web1.j2
      - index.html.web2.j2
      - nginx.conf.j2
      - nginx.yml
      - server.crt
      - server.key
   
    - I4B
      - mariadb.yml
   -.ssh-config
   -ansible.cfg
   -hosts.ini
  - Vagrantfile


# III. Repeat


## 1. NGINX

➜ **On reste dans le rôle `nginx`**, faites en sorte que :

- on puisse déclarer la liste `vhosts` en *host_vars*
- si cette liste contient plusieurs `vhosts`, le rôle les déploie tous (exemple en dessous)
- le port précisé est automatiquement ouvert dans le firewall
- vous gérez explicitement les permissions de tous les fichiers

Exemple de fichier de variable avec plusieurs Virtual Hosts dans la liste `vhosts` :

```yml
vhosts:
---
# tasks/main.yml

- name: Déclaration de la liste des vhosts en host_vars
  set_fact:
    nginx_vhosts: "{{ vhosts }}"

- name: Déploiement de tous les vhosts
  include_tasks: deploy_vhosts.yml
  loop: "{{ nginx_vhosts }}"
  loop_control:
    loop_var: vhost

- name: Trigger du handler à chaque modification de la configuration NGINX
  meta: flush_handlers

```

➜ **Ajoutez une mécanique de `handlers/`**

```
# handlers/main.yml

- name: redémarrer le service nginx
  systemd:
    name: nginx
    state: restarted
  listen: "flush_handlers"

```


## 3. Dynamic loadbalancer
web app
```
---
- name: Install nginx
  yum:
    name: nginx
    state: present

- name: Copy index.html
  template:
    src: index.html.j2
    dest: /usr/share/nginx/html/index.html

- name: Start nginx
  systemd:
    name: nginx
    state: started
    enabled: yes
```
proxy
```
---
- name: Install nginx
  yum:
    name: nginx
    state: present

- name: Copy nginx config
  template:
    src: nginx.conf.j2
    dest: /etc/nginx/conf.d/loadbalancer.conf
  notify: restart nginx

- name: Start nginx
  systemd:
    name: nginx
    state: started
    enabled: yes
```



