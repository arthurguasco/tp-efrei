Voici le compte rendu de mes TP.
***
- [TP1 : Programmatic provisioning](https://gitlab.com/arthurguasco/tp-efrei/-/blob/main/TP1%20:%20Programmatic%20provisioning/README.md?ref_type=heads)  
- [TP2 : Network boot](https://gitlab.com/arthurguasco/tp-efrei/-/blob/main/TP2%20:%20Network%20boot/README.md?ref_type=heads)  
- [TP4 : Automatisation et gestion de conf](https://gitlab.com/arthurguasco/tp-efrei/-/blob/main/TP4%20:%20Automatisation%20et%20gestion%20de%20conf/README.md?ref_type=heads)

