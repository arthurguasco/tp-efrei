# TP1 : Programmatic provisioning



# I. Une première VM

## 1. ez startup


🌞 **`Vagrantfile` dans le dépôt git de rendu SVP !**
```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian12"
end
```


## 2. Un peu de conf


🌞 **Ajustez le `Vagrantfile` pour que la VM créée** :

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian12"

  config.vm.define "ezconf" do |ezconf|
    ezconf.vm.hostname = "ezconf.tp1.efrei"
    ezconf.vm.network "private_network", ip: "10.1.1.11", netmask: "255.255.255.0"
    
    ezconf.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.cpus = 2
      vb.customize ["createhd", "--filename", "ezconf_disk.vdi", "--size", "20480"]
      vb.customize ["storageattach", :id, "--storagectl", "IDE Controller", "--port", "1", "--device", "0", "--type", "hdd", "--medium", "ezconf_disk.vdi"]
    end
end


```

# II. Initialization script


🌞 **Ajustez le `Vagrantfile`** :


```Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian12"

  config.vm.define "ezconf" do |ezconf|
    ezconf.vm.hostname = "ezconf.tp1.efrei"
    ezconf.vm.network "private_network", ip: "10.1.1.11", netmask: "255.255.255.0"
    
    ezconf.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.cpus = 2
      vb.customize ["createhd", "--filename", "ezconf_disk.vdi", "--size", "20480"]
      vb.customize ["storageattach", :id, "--storagectl", "IDE Controller", "--port", "1", "--device", "0", "--type", "hdd", "--medium", "ezconf_disk.vdi"]
    end
    config.vm.provision "shell", path: "script.sh" 
end
```

script.sh
```
#!/bin/bash

sudo dnf update -y

sudo dnf install -y vim python3
```


# III. Repackaging


🌞 **Repackager la VM créée précédemment**
```
$ vagrant package --output debian.box

$ vagrant box add debian debian.box

$ vagrant box list
debian           (virtualbox, 0)

$vagrant box repackage debian           (virtualbox, 0)
```



# IV. Multi VM


🌞 **Un deuxième `Vagrantfile` qui définit** :
```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian12"
  common_settings = {
    memory: "2048"  # 2 Go de RAM par défaut
  }

  vms = [
    { name: "node1", ip: "10.1.1.101", ram: "2048" },
    { name: "node2", ip: "10.1.1.102", ram: "1024" }
  ]

  vms.each do |vm|
    config.vm.define vm[:name] do |node|
      node.vm.hostname = "#{vm[:name]}.tp1.efrei"
      node.vm.network "private_network", ip: vm[:ip]
      
      node.vm.provider "virtualbox" do |vb|
        vb.memory = vm[:ram]
      end
    end
  end
end
```
🌞 **Une fois les VMs allumées, assurez-vous que vous pouvez ping `10.1.1.102` depuis `node1`**
```
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=0.280 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=0.257 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=0.254 ms

--- 10.1.1.102 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2000ms
rtt min/avg/max/mdev = 0.254/0.263/0.280/0.012 ms
```

# V. cloud-init

## 1. Repackaging

🌞 **Repackager une box Vagrant**
```
Sudo apt install cloud-init
systemctl enable cloud-init
```
🌞 **Tester !**
```
Vagrant.configure("2") do |config|
  config.vm.box = "debian"
  config.vm.disk :dvd, name: "installer", file: "./cloud-init.iso"
  ```
end


🌞 **Tester !**
```
[vagrant@cloud ~]$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
sssd:x:998:995:User for sssd:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/:/usr/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/sbin/nologin
chrony:x:997:994:chrony system user:/var/lib/chrony:/sbin/nologin
systemd-oom:x:992:992:systemd Userspace OOM Killer:/:/usr/sbin/nologin
vagrant:x:1000:1000::/home/vagrant:/bin/bash
vboxadd:x:991:1::/var/run/vboxadd:/sbin/nologin
polkitd:x:990:990:User for polkitd:/:/sbin/nologin
rtkit:x:172:172:RealtimeKit:/proc:/sbin/nologin
geoclue:x:989:989:User for geoclue:/var/lib/geoclue:/sbin/nologin
pipewire:x:988:988:PipeWire System Daemon:/var/run/pipewire:/sbin/nologin
flatpak:x:987:987:User for flatpak system helper:/:/sbin/nologin
lala:x:1001:1001:Super adminsys:/home/lala:/bin/bash

```
