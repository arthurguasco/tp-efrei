# TP2 : Network boot

```
[vagrant@debian ~]$ sudo apt install dhcp-server
```


# I. Installation d'un serveur DHCP

🌞 **Configurer le serveur DHCP**

```
[vagrant@tp2 ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;

option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet 192.168.1.0 netmask 255.255.255.0 {
    
    range 192.168.1.100 192.168.1.200;

    class "pxeclients" {
        match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
        next-server 192.168.1.10; 

        if option architecture-type = 00:07 {
            filename "BOOTX64.EFI";
        }
        else {
            filename "pxelinux.0";
        }
    }
}

```

🌞 **Démarrer le serveur DHCP**


```

[vagrant@tp2 ~]$ sudo systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; preset: disabled)
     Active: active (running) since Thu 2024-04-04 15:36:14 UTC; 1h 41min ago
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)
   Main PID: 2853 (dhcpd)
     Status: "Dispatching packets..."
      Tasks: 1 (limit: 12264)
     Memory: 4.5M
        CPU: 11ms
     CGroup: /system.slice/dhcpd.service
             └─2853 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid

Apr 04 15:36:14 tp2.leo dhcpd[2853]:
Apr 04 15:36:14 tp2.leo dhcpd[2853]: No subnet declaration for eth0 (10.0.2.15).
Apr 04 15:36:14 tp2.leo dhcpd[2853]: ** Ignoring requests on eth0.  If this is not what
Apr 04 15:36:14 tp2.leo dhcpd[2853]:    you want, please write a subnet declaration
Apr 04 15:36:14 tp2.leo dhcpd[2853]:    in your dhcpd.conf file for the network segment
Apr 04 15:36:14 tp2.leo dhcpd[2853]:    to which interface eth0 is attached. **
Apr 04 15:36:14 tp2.leo dhcpd[2853]:
Apr 04 15:36:14 tp2.leo dhcpd[2853]: Sending on   Socket/fallback/fallback-net
Apr 04 15:36:14 tp2.leo dhcpd[2853]: Server starting service.
Apr 04 15:36:14 tp2.leo systemd[1]: Started DHCPv4 Server Daemon.

```

🌞 **Démarrer le socket TFTP**

```
[vagrant@tp2 ~]$ sudo systemctl status tftp.socket
● tftp.socket - Tftp Server Activation Socket
     Loaded: loaded (/usr/lib/systemd/system/tftp.socket; enabled; preset: disabled)
     Active: active (listening) since Thu 2024-04-04 16:41:37 UTC; 40min ago
      Until: Thu 2024-04-04 16:41:37 UTC; 40min ago
   Triggers: ● tftp.service
     Listen: [::]:69 (Datagram)
      Tasks: 0 (limit: 12264)
     Memory: 4.0K
        CPU: 549us
     CGroup: /system.slice/tftp.socket

Apr 04 16:41:37 tp2.leo systemd[1]: Listening on Tftp Server Activation Socket.
```

🌞 **Ouvrir le bon port firewall**

```
[vagrant@tp2 ~]$ sudo firewall-cmd --add-service=tftp --permanent
success
[vagrant@tp2 ~]$ sudo firewall-cmd --reload
success
[vagrant@tp2 ~]$

```
# III. Un peu de conf

**Conf du PXE**

```
[vagrant@tp2 ~]$ sudo cp /usr/share/syslinux/pxelinux.0  /var/lib/tftpboot/
[vagrant@tp2 ~]$ sudo mkdir -p /var/pxe/debian
[vagrant@tp2 ~]$ sudo mkdir /var/lib/tftpboot/debian
[vagrant@tp2 ~]$ sudo mount -t iso9660 -o loop,ro /home/vagrant/debian12.iso /var/pxe/debian
mount: (hint) your fstab has been modified, but systemd still uses
       the old version; use 'systemctl daemon-reload' to reload.
[vagrant@tp2 ~]$ sudo su
[root@tp2 vagrant] systemctl daemon-reload
[root@tp2 vagrant] cp /var/pxe/debian/images/pxeboot/{vmlinuz,initrd.img} /var/lib/tftpboot/rocky9/
[root@tp2 vagrant] cp /usr/share/syslinux/{menu.c32,vesamenu.c32,ldlinux.c32,libcom32.c32,libutil.c32} /var/lib/tftpboot/
[root@tp2 vagrant] mkdir /var/lib/tftpboot/pxelinux.cfg
[root@tp2 vagrant] nano /var/lib/tftpboot/pxelinux.cfg/default
```
 **Conf du `/var/lib/tftpboot/pxelinux.cfg/default`**

```
[vagrant@tp2 ~]$ cat /var/lib/tftpboot/pxelinux.cfg/default
default vesamenu.c32
prompt 1
timeout 60

display boot.msg

label linux
  menu label Install debian
  menu default
  kernel debian/vmlinuz
  append initrd=debian/initrd.img ip=dhcp inst.repo=http://192.168.1.10/debian
label rescue
  menu label Rescue installed system
  kernel debian/vmlinuz
  append initrd=debian/initrd.img rescue
label local
  menu label Boot from ^local drive
  localboot 0xffff
```

**Conf Apache**

```
[root@tp2 vagrant] cat /etc/httpd/conf.d/pxeboot.conf
Alias /debian /var/pxe/debian
<Directory /var/pxe/debian>
    Options Indexes FollowSymLinks
    Require ip 127.0.0.1 192.168.1.0/24
</Directory>
```

**Ouverture du port 80**

```
[root@tp2 vagrant] sudo firewall-cmd --add-port=80/tcp --permanent
success
[root@tp2 vagrant] sudo firewall-cmd --reload
success
```

# V.TEST
```
21:39:59.763385 IP _gateway.49679 > servPXE.ssh: Flags [.], ack 8908, win 65535, length 0
21:39:59.767767 IP servPXE.38135 > ntp1.omdc.pl.ntp: NTPv4, Client, length 48
21:39:59.800357 IP ntp1.omdc.pl.ntp > servPXE.38135: NTPv4, Server, length 48
21:39:59.863005 IP servPXE.36047 > 10.0.2.3.domain: 37989+ PTR? 115.117.222.92.in-addr.arpa. (45)
21:39:59.863636 IP servPXE.ssh > _gateway.49679: Flags [P.], seq 8908:8976, ack 1, win 62780, length 68   
21:39:59.864396 IP _gateway.49679 > servPXE.ssh: Flags [.], ack 8976, win 65535, length 0
21:39:59.913737 IP 10.0.2.3.domain > servPXE.36047: 37989 1/0/0 PTR ntp1.omdc.pl. (71)
```
